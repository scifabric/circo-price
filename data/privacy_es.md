# Política de privacidad
Lea detenidamente esta política de privacidad ("Política de Privacidad").

Al acceder a este sitio web, dicha "Política de Privacidad" se aplicará automáticamente. Al crear una identidad en este sitio web, usted se convierte en colaborador y como colaborador, acepta y da su consentimiento a los términos descritos en esta "Política de Privacidad". Esta política entra en vigor el [28 de marzo de 2018].


## Enmiendas y actualizaciones a esta política
Este sitio web puede modificar su "Política de Privacidad" cada cierto tiempo para reflejar los requerimientos de nuevas normativas o cambios en nuestras prácticas de captación, uso o divulgación de información. Si se modifica la "Política de Privacidad" se anotará aquí la nueva fecha de entrada en vigor. Por lo tanto, debe visitar esta página regularmente para ver la "Política de Privacidad" más actualizada.


## Enlaces a otros sitios web
Es obvio, pero cabe recordar que esta "Política de Privacidad" no se aplica a los sitios web de terceros a los que se accede a través de enlaces desde esta página web. Este página web no tiene control sobre lo que dichos sitios hacen con la información capturada, por lo que sería absurdo afirmar que esta página web tiene responsabilidad alguna sobre las mismas. Si desea estar protegido, lea detenidamente las políticas de privacidad de terceros antes de aceptar sus términos o de utilizar dichas páginas web.


## Presentación de información personal

La naturaleza y propósito de este sitio web es de una plataforma abierta, donde ayudar a analizar tejados de los edificios públicos de España.

Este sitio web está diseñado para permitirle a usted, y a otros usuarios y colaboradores, compartir información de modo abierto y transparente, con la excepción de información recopilada automáticamente y detallada a continuación, que puede incluir información personal. Esta página web, como norma general, no espera ni tiene la intención de que usted envíe información personal o privada. No obstante lo anterior, si debiera enviar información personal por cualquier razón legítima; dicha información se trataría en conformidad con los requisitos legales relativos a la información personal, pudiendo incluir requisitos legales para que la información se ponga a disposición de organismos encargados de hacer cumplir la ley o terceros.


### ¿Qué es información personal?

Información personal significa cualquier información relacionada con una persona física, que puede identificarse, directa o indirectamente, y en particular mediante referencia a un número de identificación o uno o más factores específicos de su vida física, psicológica, mental, económica, cultural o identidad social.


### Recopilación automática de información
Este sitio web recibe información de identificación no personal a través de la dirección IP de PYBOSSA para navegar o participar en el proyecto. Su IP se encripta dentro del sistema, ocultando su dirección IP real para protegerlo.

Esta página web recibe información no personalmente identificable a través de Google Analytics, Google Analytics Demographics e Interest Reporting, como datos agregados del tipo de navegador, preferencia de idioma, sitio de referencia, así como la hora de cada visita, edad, sexo o intereses.


## Uso de información
Este sitio web utiliza su dirección de IP encriptada a través de PYBOSSA para evitar que usuarios participen dos veces en la misma tarea, y para entender cómo los usuarios utilizan este sitio, y donde sea posible, mejorar su experiencia a través de los datos de Google Analytics, Google Analytics Demographics e Interest Reporting.


## Acceso a información por terceros
Este sitio web no venderá su información personal a terceros sin su consentimiento previo. Este sitio puede revelar su información personal si la ley lo exige, y cuando esta página web considere que la divulgación es necesaria para proteger nuestros derechos y / o para cumplir con un procedimiento judicial, una orden judicial o un proceso legal.
Este sitio web puede divulgar los resultados de la información personal agregada, por ejemplo, para enseñar qué idiomas utilizan sus contribuyentes.


## Google Analytics y Opt Out
Este sitio web usa Google Analytics, quien a su vez utiliza cookies para correlacionar su comportamiento en ésta y otras páginas web.

Este sitio utiliza Google Analytics para obtener información personal no identificable a través de Google Analytics, Google Analytics Demographics e Interest Reporting, tales como datos agregados al tipo de navegador, preferencia de idioma, sitio de referencia, así como la hora de cada visita, edad, sexo o intereses.

Esta página web utiliza dicha información para comprender mejor el modo de uso de sus visitantes y, donde sea posible, mejorar su experiencia.

Si desea evitar que sus datos sean utilizados por Google Analytics en este sitio web, puede utilizar el add-on del navegador de exclusión de Google Analytics para JavaScript de Google Analytics (ga.js, analytics.js, dc.js).

Si quiere cancelar la suscripción, descargue e instale https://tools.google.com/dlpage/gaoptout/ en su navegador. El complemento de inhabilitación de Google Analytics está diseñado para ser compatible con Chrome, Internet Explorer 8-11, Safari, Firefox y Opera.

Más información sobre cómo cancelar la suscripción https://support.google.com/analytics/answer/181881?hl=en.
