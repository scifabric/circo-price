# Cómo usamos las cookies en este sitio web

Este sitio web utiliza cookies para manejar tu sesión. 

## Google Analytics cookies
Este sitio utiliza las cookies de Google Analytics para entender mejor cómo utilizas el sitio web. Nuestro principal objetivo con Google Analytics es saber cómo mejorar nuestra web para que puedas tener la mejor experiencia de usuario.

## Deshabilitar las cookies
Sin embargo, si quieres puedes deshabiliatar las cookies en este sitio (o en cualquier otro) desde tu navegador. Consulta la ayuda de tu navegador web para deshabilitar las cookies.
