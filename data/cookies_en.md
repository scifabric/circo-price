# How this project uses cookies

This site uses cookies for handling your session in the site. 

## Cookies for tutorials
The site has several tutorials and onboarding processes. We use cookies to know that you have seen the tutorials and/or onboarding modals, so you don't get them everytime.

## Google Analytics cookies
This site uses Google Analytics to understand better how you are using the site. Our main goal with Google Analytics is to learn and improve our site so you can get the best of it.

## Disabling cookies
However, if you want to disable the cookies in this site or other, we recommend you to check the help section of your web browser so you can disable them.
