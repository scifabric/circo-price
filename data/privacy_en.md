# Privacy Policy
Please read this Privacy Policy (“Privacy Policy”) carefully.

When you access this website this policy binds you. When you create an identity on this site you are a Contributor and as a Contributor you accept and consent to the practices described in this Privacy Policy. This Policy is effective as from [28/March/2018].

## Amendments and Updates to this Policy
This site may amend this Privacy Policy from time to time to reflect regulatory requirements or changes in our information collection, use or disclosure practices. If this Privacy Policy is changed, the new Effective Date will be noted here. You should therefore view this Page regularly to view our most up-to-date Privacy Policy.

## Linking Websites
It is obvious but this is a reminder that this Privacy Policy does not apply to any third-party sites accessed via links from this site. This site has no control over what those sites do with information which they collect so it would be absurd to claim that this site has any responsibility for what those sites do. If you want to protect yourself then carefully read through any third-party privacy policies before agreeing to their terms or using such third party websites.

## Submission of Personal Information:
The nature and purpose of this site is an open platform, where you can help submitting ads that you see in Facebook.

This is site is designed to allow you and other Users and Contributors to share information in an open and transparent manner. With the exception of the information collected automatically, detailed below and which may include personal information this site does not generally expect from or intend for you to submit personal or private information. Notwithstanding the above, should you submit personal information for any legitimate reason; such information will be treated in compliance with legal requirements pertaining to personal information which may include legal requirements that the information is made available to law enforcement agencies or` other third parties.

This site will ask you to fill in a survey to participate in the project. All that information is stored in your web browser encrypted by a password you create. When you submit an ad, the information is desencrypted by you and assigned to a completely random identifier that cannot be linked to you in any possible way.

### What is personal information?
Personal information means any information relating to a natural person, who can be identified, directly or indirectly, in particular by reference to an identification number or to one or more factors specific to his or her physical, physiological, mental, economic, cultural or social identity.

### Automatic collection of information


This site gets non-personally-identifying information via PYBOSSA IP address to navigate or participate in the project. Your IP is encrypted within the system, hiding your real IP address to protect you.

This site gets non-personally-identifying information via Google Analytics, Google Analytics Demographics and Interest Reporting like aggregated data of browser type, language preference, referring site, and the time of each visit, age, gender, or interests.

## Use of information
This site uses the encrypted IP address via PYBOSSA to prevent users to participate twice in the same task, and the Google Analytics, Google Analytics Demographics and Interest Reporting data to better understand how visitors use this site and, where possible, to improve their experience.

## Access to information by Third Parties
This site will not sell your personal information to third parties without your prior consent. This sitel may disclose your personal information as required by law when this site believes that disclosure is necessary to protect our rights, and/or to comply with a judicial proceeding, court order or legal process.

This site may disclose the results of aggregated personal information, for instance to show in which languages contributors use.

## Google Analytics and Opt Out
This site uses Google Analytics which uses cookies to correlate your behaviour on this site and other sites.

This site uses Google Analytics to get non-personally-identifying information via Google Analytics, Google Analytics Demographics and Interest Reporting like aggregated data of browser type, language preference, referring site, and the time of each visit, age, gender, or interests.

This site uses this information to better understand how visitors use This site and, where possible, to improve their experience.

If you want to prevent your data from this site being used by Google Analytics you can use the Google Analytics opt-out browswer add-on for the Google Analytics JavaScript (ga.js, analytics.js, dc.js).

If you want to opt-out, [download and install](https://tools.google.com/dlpage/gaoptout/) the add-on for your web browser. The Google Analytics opt-out add-on is designed to be compatible with Chrome, Internet Explorer 8-11, Safari, Firefox and Opera.

More information about how to opt-out [here](https://support.google.com/analytics/answer/181881?hl=en).
