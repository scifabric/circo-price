const ImageminPlugin = require('imagemin-webpack-plugin').default
const imageminMozjpeg = require('imagemin-mozjpeg')

module.exports = async function () {
  this.extendBuild(config => {
    // Add Imagemin plugin
    if (!process.isDev) {
      config.plugins.push(new ImageminPlugin({
        plugins: [
          imageminMozjpeg({
            quality: 65,
            progressive: true
          })
        ]
      }))
    }
  })
}
