# Frontend code for lamemoriadelcirco.es

Lamemoriadelcirco.es is a project where citizens can help to classify, tag, and describe images from the Spanish Circus.

This is the frontend server code built using Nuxt.js and PYBOSSA.

## Build Setup

``` bash
# install dependencies
$ yarn

# create a default config
$ cp localConfig.js.tmpl localConfig.js

# serve with hot reload at localhost:3000
$ npm run dev

# run dev as spa app
$ npm run dev -- --spa

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).

## Financial support

 - Ayudas para la modernización e innovación de las industrias culturales y creativas mediante proyectos digitales y tecnológicos. Convocatoria 2017
 - Ayudas para la acción y la promoción cultural. Convocatoria de 2017

