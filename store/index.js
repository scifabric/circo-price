import Vuex from 'vuex'
import mutations from '@/store/mutations.js'
import localConfig from '@/localConfig.js'
import _ from 'lodash'

const createStore = () => {
  return new Vuex.Store({
    state: {
      secretKey: null,
      currentUser: {},
      locales: localConfig.locales,
      transitionName: 'zoom',
      locale: 'es',
      tasks: [],
      showNav: false,
      transparentNav: false,
      currentModal: null,
      currentModalActive: false,
      currentModalProps: {},
      globalModalProps: {},
      projectNav: false,
      project: {},
      category: {},
      bottomNavbar: false,
      postTaskRun: false,
      disablePostTaskRun: true,
      disableNextStep: true,
      disablePrevStep: true,
      bottomNavbarButtons: ['skip'],
      projectStep: 1,
      taskrunInfo: {},
      activeQuestion: '',
      userConsent: false,
      auth: {},
      showQuestions: true,
      facebook: {
        ads: []
      },
      projectTutorialStep: 3,
      projectTutorial: false,
      projectNavTutorial: console.log,
      showHelpButton: false,
      userContribs: 0,
      isLoading: false,
      lastTutorialStepProps: {
        position: 'top-right',
        step: 3,
        totalSteps: 3,
        customClass: 'is-orange',
        content: 'Click on help icon to display this tutorial again. Go for it! :)'
      }
    },
    getters: {
      categoryStats: state => {
        let stats = {
          n_tasks: (_.sumBy(state.projects, 'stats.n_tasks') / 5),
          n_volunteers: _.sumBy(state.projects, 'stats.n_volunteers'),
          n_task_runs: (_.sumBy(state.projects, 'stats.n_task_runs') - 186)
        }
        return stats
      },
      completedTasks: state => {
        return _.filter(state.tasks, {state: 'completed'})
      },
      ongoingTasks: state => {
        return _.filter(state.tasks, function (o) {
          return (o.state === 'ongoing' && Object.keys(o.taskrun).length === 0)
        })
      },
      pendingTasks: state => {
        return _.filter(state.tasks, function (o) {
          return (o.state === 'ongoing' && Object.keys(o.taskrun).length > 0)
        })
      },
      projectCategory: state => {
        if (state.project.hasOwnProperty('id')) {
          return _.find(state.categories, {id: state.project.category_id})
        } else {
          return null
        }
      },
      userConsent: state => {
        return state.userConsent
      },
      currentUser: state => {
        return state.currentUser
      },
      currentTask: state => {
        if (state.tasks.length) {
          return state.tasks[0]
        } else {
          return {}
        }
      },
      showNav: state => {
        return state.showNav
      },
      transparentNav: state => {
        return state.transparentNav
      }
    },
    mutations,
    actions: {
      async nuxtServerInit ({ dispatch }, { req }) {
        await dispatch('getUser')
        await dispatch('getCategories')
        // await dispatch('getSocialLogins')
        await dispatch('getProjects')
      },
      async getUser ({ commit }) {
        const data = await this.$axios.$get('/account/profile')
        if (data.hasOwnProperty('user')) {
          commit('LOGIN', data.user)
        } else {
          commit('LOGOUT')
        }
      },
      async getCategories ({ commit }) {
        const data = await this.$axios.$get('/api/category?all=1')
        commit('ADD_CATEGORIES', data)
      },
      async getSocialLogins ({ commit }) {
        const data = await this.$axios.$get('/account/signin')
        commit('ADD_SOCIAL_LOGINS', data.auth || {})
      },
      async getProjects ({commit}) {
        let url = '/api/category?short_name=uno&all=1'
        let data = await this.$axios.$get(url)
        url = `/api/project?category_id=${data[0].id}&stats=1`
        data = await this.$axios.$get(url)
        if (data.length) {
          commit('set', {key: 'projects', val: data})
        }
        let p = _.filter(data, {info: {'task_presenter': 'artistas'}})
        commit('set', {key: 'project', val: p})
      }
    }
  })
}

export default createStore
