import Vue from 'vue'
import _ from 'lodash'

export default {
  LOGIN: (state, user) => {
    state.currentUser = user
  },
  LOGOUT: (state, user) => {
    state.currentUser = {}
  },
  updateUserProfile: (state, data) => {
    state.currentUser[data.field] = data.value
  },
  SET_LANG: (state, data) => {
    if (data.hasOwnProperty('locale') && state.locales.indexOf(data.locale) !== -1) {
      state.locale = data.locale
      if (data.hasOwnProperty('i18n')) {
        data.i18n.locale = data.locale
      }
    }
  },
  ADD_TASKS: (state, tasks) => {
    var tmp = _.union(state.tasks, tasks)
    state.tasks = tmp
  },
  DEL_TASK: (state, task) => {
    state.tasks = _.reject(state.tasks, {id: task.id})
  },
  RESET_TASKS: (state) => {
    state.tasks = []
  },
  TOGGLE_NAV: (state) => {
    state.showNav = !state.showNav
  },
  TOGGLE_PROJECT_NAV: (state) => {
    state.projectNav = !state.projectNav
  },
  SET_PROJECT_NAV: (state, value) => {
    state.projectNav = value
  },
  SET_TRANSPARENT_NAV: (state, value) => {
    state.transparentNav = value
  },
  SET_MODAL: (state, value) => {
    state.currentModal = value
  },
  SET_MODAL_PROPS: (state, value) => {
    state.currentModalProps = value
  },
  SET_GLOBAL_MODAL_PROPS: (state, value) => {
    state.globalModalProps = value
  },
  SET_MODAL_ACTIVE: (state, value) => {
    state.currentModalActive = value
  },
  UPDATE_USER_META: (state, data) => {
    Vue.set(state.currentUser.info, data.key, data.value)
  },
  UPDATE_USER_BASIC: (state, value) => {
    state.currentUser = _.assign(state.currentUser, value)
  },
  UPDATE_PROJECT: (state, value) => {
    Vue.set(state, 'project', value)
  },
  UPDATE_PROJECT_CATEGORY: (state, value) => {
    Vue.set(state, 'category', value)
  },
  ADD_CATEGORIES: (state, value) => {
    Vue.set(state, 'categories', value)
  },
  BOTTOM_NAVBAR: (state, value) => {
    state.bottomNavbar = value
  },
  POST_TASK_RUN: (state, value) => {
    state.postTaskRun = value
  },
  DISABLE_POST_TASK_RUN: (state, value) => {
    state.disablePostTaskRun = value
  },
  ADD_AD: (state, value) => {
    state.facebook.ads.push(value)
  },
  RESET_ADS: (state, value) => {
    Vue.set(state, 'facebook.ads', [])
  },
  DEL_AD: (state, value) => {
    state.facebook.ads = _.reject(state.facebook.ads, value)
  },
  ADD_SOCIAL_LOGINS: (state, value) => {
    state.auth = value
  },
  UPDATE_BOTTOM_NAVBAR_BUTTONS: (state, value) => {
    Vue.set(state, 'bottomNavbarButtons', value)
  },
  RESET_BOTTOM_NAVBAR_BUTTONS: (state) => {
    Vue.set(state, 'bottomNavbarButtons', ['skip'])
  },
  NEXT_PROJECT_STEP: (state, value) => {
    state.projectStep += 1
    state.disableNextStep = true
  },
  PREV_PROJECT_STEP: (state, value) => {
    if ((state.projectStep - 1) > 1) {
      state.projectStep -= 1
    } else {
      state.projectStep = 1
    }
    state.disablePrevStep = true
  },
  DISABLE_NEXT_STEP: (state, value) => {
    state.disableNextStep = value
  },
  DISABLE_PREV_STEP: (state, value) => {
    state.disablePrevStep = value
  },
  RESET_STEP: (state) => {
    state.projectStep = 1
    state.disableNextStep = true
  },
  UPDATE_TASKRUN_INFO: (state, value) => {
    Vue.set(state, 'taskrunInfo', value)
  },
  RESET_TUTORIAL_STEP: (state) => {
    state.projectTutorialStep = 1
  },
  INCREASE_TUTORIAL_STEP: (state, value) => {
    state.projectTutorialStep += 1
  },
  DECREASE_TUTORIAL_STEP: (state, value) => {
    state.projectTutorialStep -= 1
  },
  SET_TUTORIAL_STEP: (state, value) => {
    state.projectTutorialStep = value
  },
  UPDATE_PROJECT_TUTORIAL: (state, value) => {
    state.projectTutorial = value
  },
  SET_NAV_PROJECT_TUTORIAL: (state, value) => {
    state.projectNavTutorial = value
  },
  UPDATE_SHOW_HELP_BUTTON: (state, value) => {
    state.showHelpButton = value
  },
  SET_LAST_TUTORIAL_STEP: (state, value) => {
    state.lastTutorialStepProps = value
  },
  SET_USER_CONSENT: (state, value) => {
    state.userConsent = value
  },
  SET_PROJECT_STEP: (state, step) => {
    state.projectStep = step
  },
  set: (state, obj) => {
    Vue.set(state, obj.key, obj.val)
  },
  increaseUserContribs: (state) => {
    state.userContribs += 1
  }
}
