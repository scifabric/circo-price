const localConfig = require('./localConfig')
const PurgecssPlugin = require('purgecss-webpack-plugin')
const glob = require('glob-all')
const path = require('path')

module.exports = {
  /*
  ** Headers of the page
  */
  plugins: ['~/plugins/vue-buefy.js', '~/plugins/filters.js', '~/plugins/i18n.js',
    {src: '~/plugins/vue-touch.js', ssr: false},
    {src: '~/plugins/vue-media-queries.js', ssr: false},
    {src: '~/plugins/vue-scrollto.js', ssr: false},
    {src: '~/plugins/carousel.js', ssr: false},
    {src: '~/plugins/preauth.js', ssr: true},
    {src: '~/plugins/share.js', ssr: true},
    {src: '~/plugins/localstorage.js', ssr: false}
  ],

  head: {
    title: 'pybossa-nuxt',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'PYBOSSA Nuxt.js frontend' }
    ],
    link: [
      { rel: 'icon', type: 'image/png', href: '/favicon.png' },
      { rel: 'stylesheet', href: '//cdn.materialdesignicons.com/2.0.46/css/materialdesignicons.min.css' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Abril+Fatface|Open+Sans|Roboto+Condensed|Bevan' }
    ]
  },
  /*
  ** CSS, see https://nuxtjs.org/api/configuration-css
  */
  css: [
    { src: '@/assets/main.scss', lang: 'scss'}
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#302287', height: '5px' },
  /*
  ** Build configuration
  */
  build: {
    extractCSS: true,
    /*
    ** Run ESLint on save
    */
    extend (config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
      /* add the following */
      config.module.rules.splice(0, 0, {
        test: /\.js$/,
        include: [path.resolve(__dirname, './node_modules/vue2-google-maps')],
        loader: 'babel-loader'
      })
      if (!ctx.isDev) {
        // Remove unused CSS using purgecss. See https://github.com/FullHuman/purgecss
        // for more information about purgecss.
        config.plugins.push(
          new PurgecssPlugin({
            paths: glob.sync([
              path.join(__dirname, './pages/**/*.vue'),
              path.join(__dirname, './layouts/**/*.vue'),
              path.join(__dirname, './components/**/*.vue')
            ]),
            whitelist: ['html', 'body']
          })
        )
      }
    },
    /*
    ** Remove bulma postcss warnings
    */
    postcss: {
      plugins: {
        'postcss-custom-properties': false
      }
    },
    vendor: ['buefy', 'vue-i18n', 'lodash', 'vue-touch', 'vue-carousel']
  },
  router: {
    middleware: ['i18n']
  },
  modules: [
    '@nuxtjs/google-analytics',
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
    '@nuxtjs/pwa',
    '@nuxtjs/markdownit',
    '~/modules/imagemin.js'
  ],
  'google-analytics': {
    id: localConfig.GoogleAnalytics
  },
  manifest: localConfig.manifest,
  markdownit: {
    preset: 'default',
    linkify: true,
    breaks: true,
    injected: true
  },
  axios: {
    baseURL: localConfig.baseURL,
    withCredentials: true,
    credentials: true,
    proxyHeaders: true,
    requestInterceptor: (config) => {
      config.headers['Content-Type'] = 'application/json'
      if (config.data && config.data.hasOwnProperty('csrf')) {
        config.headers['X-CSRFToken'] = config.data.csrf
      }
      if (config.data === undefined) {
        config.data = {}
      }
      if (config.data && Object.prototype.toString.call(config.data) === '[object FormData]') {
        config.headers['X-CSRFToken'] = config.data.get('csrf')
      }
      return config
    }
  },
  proxy: {
    '/api': localConfig.baseURL
  }
}
