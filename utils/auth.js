export const updateSession = (user, cookieStr) => {
  let allCookies = []
  if (cookieStr) {
    allCookies = cookieStr.split(';')
  }

  let hasSession = false
  let sessionName = null

  // Get the username of the current session, if any
  for (let cookie of allCookies) {
    if (cookie.indexOf('remember_token=') > -1) {
      hasSession = true
      sessionName = cookie.split('|')[0].split('=')[1]
    }
  }

  // Return true if the user doesn't match the current session
  console.log(user)
  console.log(hasSession)
  console.log(((user === null || user === undefined) === hasSession) ||
    (hasSession && user.name !== sessionName))

  return (
    ((user === null || user === undefined) === hasSession) ||
    (hasSession && user.name !== sessionName)
  )
}
