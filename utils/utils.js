export const preloadContributed = (tasks, trs) => {
  for (let task of tasks) {
    if (task.state === 'ongoing') {
      let def = {}
      for (let tr of trs) {
        let t = task.task_runs.filter(function (o) {
          return o.id === tr
        })
        if (t.length === 1) {
          t = t[0]
          def = t.info
          break
        }
      }
      task.taskrun = def
    }
  }
  return tasks
}
