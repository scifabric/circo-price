export const ord = (num) => {
  let nums = {
    1: 'primer',
    2: 'segundo',
    3: 'tercero',
    4: 'cuarto',
    5: 'quinto',
    6: 'sexto',
    7: 'séptimo',
    8: 'octavo',
    9: 'noveno',
    10: 'décimo',
    11: 'undécimo',
    12: 'duodécimo'
  }

  if (num < 12) {
    return nums[num]
  }

  if (num >= 13 && num < 20) {
    let i = num.toString()[1]
    return `décimo ${nums[i]}`
  }

  if (num >= 21 && num < 30) {
    let i = num.toString()[1]
    return `vigésimo ${nums[i]}`
  }

  if (num >= 31 && num < 40) {
    let i = num.toString()[1]
    return `trigésimo ${nums[i]}`
  }

  if (num >= 41 && num < 50) {
    let i = num.toString()[1]
    return `cuatrigésimo ${nums[i]}`
  }

  if (num >= 51 && num < 60) {
    let i = num.toString()[1]
    return `quincuagésimo ${nums[i]}`
  }

  if (num >= 61 && num < 70) {
    let i = num.toString()[1]
    return `sexagésimo ${nums[i]}`
  }

  if (num >= 71 && num < 80) {
    let i = num.toString()[1]
    return `septuagésimo ${nums[i]}`
  }

  if (num >= 81 && num < 90) {
    let i = num.toString()[1]
    return `octogésimo ${nums[i]}`
  }

  if (num >= 91 && num < 100) {
    let i = num.toString()[1]
    return `nonagésimo ${nums[i]}`
  }
  return false
}
