export default {
  data () {
    return {formGetUrl: '',
      csrf: '',
      errors: {},
      form: {},
      upload_form: {},
      password_form: {},
      loading: false,
      files: []}
  },
  computed: {
    currentUser () {
      return this.$store.getters.currentUser
    },
    cleanUserAPI () {
      var tmp = JSON.parse(JSON.stringify(this.currentUser))
      delete tmp.rank
      delete tmp.n_answers
      delete tmp.registered_ago
      delete tmp.score
      delete tmp.total
      delete tmp.csrf
      delete tmp.btn
      return tmp
    }
  },
  methods: {
    async getCsrf () {
      const data = await this.$axios.$get(this.$route.fullPath)
      if (data.hasOwnProperty('form')) {
        this.csrf = data.form.csrf
        this.form = data.form
      }
      if (data.hasOwnProperty('upload_form')) {
        this.upload_csrf = data.upload_form.csrf
        this.upload_form = data.upload_form
      }
    },
    errorsFmt (field) {
      if (this.errors.hasOwnProperty(field)) {
        return this.errors[field][0]
      } else {
        return ''
      }
    },
    fieldType (field) {
      if (this.errors.hasOwnProperty(field)) {
        return 'is-danger'
      } else {
        return ''
      }
    },
    fuzzyBoolean (value) {
      let val = value.toLowerCase()
      if (val === 'yes' || val === 'on') {
        val = true
      }
      if (val === 'no' || val === 'off') {
        val = false
      }
      return val
    },
    async post (data) {
      delete data.errors
      let url = `${this.$route.fullPath}?response_format=json`
      if (data.hasOwnProperty('postURL')) {
        url = `${data.postURL}?response_format=json`
        delete data.postURL
      }
      const res = await this.$axios.$post(url, data)

      if (res.hasOwnProperty('form') && res.form.errors.length) {
        this.errors = res.form.errors
      }

      if (res.hasOwnProperty('password_form') && Object.keys(res.password_form.errors).length) {
        this.errors = res.password_form.errors
      }
      if (res.hasOwnProperty('flash')) {
        var style = 'is-danger'
        if (res.status === 'success') style = 'is-success'
        this.$toast.open({type: style, position: 'is-top', message: res.flash})
      }
    },
    async postImage (data, msg) {
      delete data.errors
      let url = `${this.$route.fullPath}?response_format=json`
      const res = await this.$axios.$post(url, data, {headers: { 'Content-Type': 'multipart/form-data' }})
      if (res.hasOwnProperty('upload_form') && res.upload_form.errors.length) {
        if (res.hasOwnProperty('flash')) {
          this.$toast.open({type: 'is-danger', position: 'is-top', message: res.flash})
        }
        this.errors = res.upload_form.errors
      } else {
        this.$toast.open({type: 'is-success', position: 'is-top', message: msg})
        this.cropper.destroy()
      }
      this.imageCrop = null
      this.cropper.destroy()
      this.cropper = null
      this.files = []
      this.loading = false
      return res
    }
  }
}
