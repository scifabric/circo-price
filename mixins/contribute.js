import {preloadContributed} from '~/utils/utils.js'
export default {
  methods: {
    async contribute () {
      this.$store.commit('set', {key: 'isLoading', val: true})
      let url = `api/task?info={"dhash": "${this.$store.state.dhash}"}&related=true&all=1`
      let tasks = await this.$axios.$get(url)
      let key = 'contributed'
      let trs = localStorage.getItem(key) ? JSON.parse(localStorage.getItem(key)) : []
      tasks = preloadContributed(tasks, trs)
      this.$store.commit('set', {key: 'tasks', val: tasks})
      this.$router.push({name: 'contribute-dhash', params: {dhash: this.$store.state.dhash}})
      this.$store.commit('set', {key: 'isLoading', val: false})
    }
  }
}
