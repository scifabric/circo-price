import localConfig from '~/localConfig.js'
import Tutorial from '@/components/tutorial.vue'
import UIDGenerator from 'uid-generator'

export default {
  components: { Tutorial },
  data () {
    return {
      answers: [],
      title: localConfig.brand,
      meta: { hid: 'description', name: 'description', content: 'My custom description' }
    }
  },
  head () {
    return {
      title: this.title,
      meta: [
        this.meta
      ]
    }
  },
  computed: {
    external_uid: {
      get () {
        if (localStorage.getItem('external_uid') === null) {
          let uid = new UIDGenerator()
          localStorage.setItem('external_uid', uid.generateSync())
        }
        return localStorage.getItem('external_uid')
      }
    },
    currentUser () {
      return this.$store.state.currentUser
    },
    isAuth () {
      if (this.currentUser.hasOwnProperty('id')) {
        return true
      } else {
        return false
      }
    },
    currentTask () {
      return this.$store.getters.currentTask
    },
    tasks () {
      return this.$store.state.tasks
    }
  },
  methods: {
    resetAnswers () {
      this.answers = []
    },
    async signTask (projectShortName, taskId) {
      var url = `project/${projectShortName}/task/${taskId}`
      await this.$axios.$get(url)
    },
    async newTask (projectId, filters) {
      if (filters.length) {
        for (var filter of filters) {
          var query = ''
          Object.entries(filter).forEach(
            ([key, val]) => {
              if (query === '') {
                query = `${key}::${val}`
              } else {
                query += `|${key}::${val}`
              }
            }
          )
          let url = `api/task?project_id=${projectId}&external_uid=${this.external_uid}&info=${query}&fulltextsearch=true&participated=true`
          let tasks = await this.$axios.$get(url)
          this.$store.commit('ADD_TASKS', tasks)
        }
      } else {
        let url = `api/task?project_id=${projectId}&external_uid=${this.external_uid}&participated=true`
        let tasks = await this.$axios.$get(url)
        this.$store.commit('ADD_TASKS', tasks)
      }
    }
  }
}
