import _ from 'lodash'
export default {
  methods: {
    typeOfTask (task) {
      let p = _.filter(this.$store.state.projects, {id: task.project_id})
      if (p.length) {
        p = p[0]
      }
      return p.info.task_presenter
    },
    getTask (tasks, taskId) {
      return _.findIndex(tasks, {id: taskId})
    },
    async _post (data) {
      let url = 'api/taskrun'
      let taskrun = {
        project_id: this.$store.state.activeTask.project_id,
        task_id: this.$store.state.activeTask.id,
        info: data
      }
      try {
        let tr = await this.$axios.$post(url, taskrun)
        let key = 'contributed'
        let tmp = localStorage.getItem(key) ? JSON.parse(localStorage.getItem(key)) : []
        tmp.push(tr.id)
        localStorage.setItem('contributed', JSON.stringify(tmp))
        return true
      } catch (err) {
        console.log(err)
        return false
      }
    },
    async _save (data) {
      this.$store.commit('set', {key: 'transitionName', val: 'zoomout'})
      let idx = this.getTask(this.$store.state.tasks, this.$store.state.activeTask.id)
      let tmp = _.clone(this.$store.state.tasks)
      tmp[idx].taskrun = data
      this.$store.commit('set', {key: 'tasks', val: tmp})
      this.step = 0
      this.$store.commit('set', {key: 'activeQuestion', val: ''})
      this.$store.commit('set', {key: 'showQuestions', val: true})
      this.$store.commit('set', {key: 'showCheck', val: true})
      if (await this._post(data)) {
        this.$toast.open({
          message: this.$t('price.done'),
          type: 'is-success'
        })
        this.$store.commit('increaseUserContribs')
      } else {
        this.$toast.open({
          message: this.$t('price.error'),
          type: 'is-danger'
        })
      }
      this.$router.push({
        name: 'contribute-dhash',
        params: {
          dhash: this.$store.state.dhash
        }
      })
    }
  }
}
