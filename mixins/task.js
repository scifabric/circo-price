export default {
  watch: {
    tasks () {
      if (this.tasks.length >= 1 && this.tasks.length < 3) {
        this.newTask(this.project_id, [])
      }
    }
  },
  async created () {
    this.$store.commit('SET_TRANSPARENT_NAV', true)
    if (this.currentTask.hasOwnProperty('id') === false) {
      var url = `api/task/${this.$route.params.id}`
      var task = await this.$axios.$get(url)
      this.$store.commit('ADD_TASKS', [task])
    }
    this.project_id = this.currentTask.project_id
  }
}
