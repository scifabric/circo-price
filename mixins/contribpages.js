export default {
  created () {
    if (this.$store.state.tasks.length === 0 || this.$store.state.activeTask === undefined) {
      this.$router.push({name: 'contribute-dhash', params: {dhash: this.$route.params.dhash}})
    }
  }
}
