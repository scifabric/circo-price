export default {
  computed: {
    filters () {
      var tmp = []
      for (var from of this.currentUser.info.languages) {
        for (var to of this.currentUser.info.languages) {
          if (from !== to) {
            tmp.push({from: from.localeId, to: to.localeId})
          }
        }
      }
      return tmp
    }
  }
}
