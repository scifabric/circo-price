import { updateSession } from '~/utils/auth.js'

export default function ({isClient, store, req, app}) {
  const currentUser = store.state.currentUser
  let update = false
  if (isClient) {
    console.log('isClient')
    update = updateSession(currentUser, document.cookie)
  } else if (req && 'cookie' in req.headers) {
    console.log('SSR')
    update = updateSession(currentUser, req.headers.cookie)
  }
  // store.dispatch('getUser')
  if (update) {
    store.dispatch('getUser')
  }
}
