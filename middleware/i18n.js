export default function ({ isHMR, app, store, route, params, error, redirect }) {
  // If middleware is called from hot module replacement, ignore it
  if (isHMR) return
  // Set locale
  store.commit('SET_LANG', {locale: store.state.currentUser.locale})
  app.i18n.locale = store.state.locale
}
