import Vue from 'vue'
import VueTruncate from 'vue-truncate-filter'
import marked from 'marked'

Vue.use(VueTruncate)

Vue.filter('marked', val => marked(val))
