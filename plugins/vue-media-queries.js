import Vue from 'vue'
import { MediaQueries, CommonBands } from 'vue-media-queries'

const mediaQueries = new MediaQueries({
  bands: CommonBands.Bulma
})

Vue.use(mediaQueries)

Vue.mixin({
  computed: CommonBands.Bulma.mixin.computed,
  mediaQueries: mediaQueries
})
