import Vue from 'vue'
import * as VueGoogleMaps from '~/node_modules/vue2-google-maps/src/main'
import localConfig from '~/localConfig.js'

Vue.use(VueGoogleMaps, {
  load: {
    key: localConfig.GoogleMapsApiKey,
    libraries: 'places,drawing,visualization,geometry'
  }
})
