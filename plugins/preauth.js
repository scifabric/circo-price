export default function ({ app, store }) {
  app.router.onReady(() => {
    if (!store.state.currentUser.hasOwnProperty('id')) {
      store.dispatch('getUser')
    }
  })
}
